This directory contains the vardump module, a contributed module for
Drupal (drupal.org)

Note: this module's functionality has been superseded by the
variable_dump module.

Once installed and enabled, the vardump module allows a sufficiently
priviledged user to dump the site's variable table, to the screen or
to a file.

To install: unpack the module and move it to the site's modules
directory.  Navigate to admin/build/modules and enable the vardump
module. 

To use the module: once the module is installed, you can grant 'dump
variable table' permissions to a role.  Users in this role will see 
a 'Variable dump' menu item under admin/settings.  Selecting this
(navigating to admin/settings/vardump) displays a form with options
for output format and location.

Please request features and report any issues at
http://drupal.org/project/vardump


